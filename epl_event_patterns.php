<?php include 'includes/header.php'; ?>
			    <div class="content">
			        <div class="container-fluid">
			            <div class="row">
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-success text-center">
			                                        <i class="ti-check"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Deployed / Undeployed EPL</p>
			                                        <span id="deployment-epl-event-patterns"></span>
			                                        <img class="esper-epl-event-patterns-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-danger text-center">
			                                        <i class="fa fa-times"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>In Esper / <br class="visible-lg">Not in Esper EPL</p>
			                                        <span id="inesper-epl-event-patterns"></span>
			                                        <img class="esper-epl-event-patterns-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-info text-center">
			                                        <i class="ti-server"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>EPL Event Patterns Total</p>
			                                        <span id="total-epl-event-patterns"></span>
			                                        <img class="esper-epl-event-patterns-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-success text-center">
			                                        <i class="fas fa-server"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Incorrect EPL Event Patterns</p>
			                                        <span id="incorrect-epl-event-patterns"></span>
			                                        <img class="mongo-epl-event-patterns-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="row">
			                <div class="col-md-6">
			                	<div class="row">
			                		<div class="col-md-12">
					                    <div class="card">
					                        <div class="header">
					                            <h4 class="title">Deployment Stats Chart</h4>
					                        </div>
					                        <div class="content">
					                            <div id="epl-event-patterns-deployment-chart" class="ct-chart ct-perfect-fourth">
													<img class="esper-epl-event-patterns-loading img-center" src="assets/img/loader.gif" width="25%">
					                            </div>
					                            <div class="footer">
					                                <div class="chart-legend">
					                                    <i class="fa fa-circle text-info"></i> Deployed
					                                    <i class="fa fa-circle text-danger"></i> Undeployed
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					            <div class="row">
			                		<div class="col-md-12">
					                    <div class="card ">
					                        <div class="header">
					                            <h4 class="title">Esper Engine Stats</h4>
					                        </div>
					                        <div class="content">
					                        	<img class="esper-epl-event-patterns-loading img-center" src="assets/img/loader.gif" width="25%">
					                            <div class="table-responsive" id="esper-engine-epl-event-patterns-table">
												    <table class="table">
												        <thead>
												            <tr>
												                <th scope="col">Status</th>
												                <th scope="col">EPL Event Patterns</th>
												                <th scope="col">Percentage</th>
												            </tr>
												        </thead>
												        <tbody id="esper-engine-epl-event-patterns-table-body"></tbody>
												    </table>
												</div>
					                        </div>
					                    </div>
					                </div>
					            </div>
			                </div>
			                <div class="col-md-6">
			                	<div class="row">
					            	<div class="col-md-12">
					                    <div class="card ">
					                        <div class="header">
					                            <h4 class="title">Deployment Stats</h4>
					                        </div>
					                        <div class="content">
					                        	<img class="esper-epl-event-patterns-loading img-center" src="assets/img/loader.gif" width="25%">
					                            <div class="table-responsive">
												    <table class="table" id="esper-deployment-epl-event-patterns-table">
												        <thead>
												            <tr>
												                <th scope="col">Status</th>
												                <th scope="col">EPL Event Patterns</th>
												                <th scope="col">Percentage</th>
												            </tr>
												        </thead>
												        <tbody id="esper-deployment-epl-event-patterns-table-body"></tbody>
												    </table>
												</div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					            <div class="row">
					            	<div class="col-md-12">
					                    <div class="card">
					                        <div class="header">
					                            <h4 class="title">Esper Engine Chart</h4>
					                        </div>
					                        <div class="content">
					                            <div id="epl-event-patterns-inesper-chart" class="ct-chart ct-perfect-fourth">
													<img class="esper-epl-event-patterns-loading img-center" src="assets/img/loader.gif" width="25%">
					                            </div>
					                            <div class="footer">
					                                <div class="chart-legend">
					                                    <i class="fa fa-circle text-info"></i> In Esper
					                                    <i class="fa fa-circle text-danger"></i> Not In Esper
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					            	</div>
					            </div>
			                </div>
			            </div>
			            <div class="row">
			                <div class="col-md-12">
			                    <div class="card ">
			                        <div class="header">
			                            <h4 class="title text-center"><strong>Last 10 Incorrect EPL Event Patterns</strong> <a class="download-link" onclick="generateCSV(2);" data-toggle="tooltip" data-placement="top" title="Export complete historic in CSV"><i class="fas fa-download"></i></a></h4>
			                        </div>
			                        <div class="content">
			                        	<img class="mongo-epl-event-patterns-loading img-center" src="assets/img/loader.gif" width="25%">
			                            <div class="table-responsive" id="mongo-incorrect-epl-event-patterns-table">
										    <table class="table">
										        <thead>
										            <tr>
										                <th scope="col" width="25%">Name</th>
										                <th scope="col">Content</th>
										                <th scope="col" width="25%">Exception date</th>
										            </tr>
										        </thead>
										        <tbody id="mongo-incorrect-epl-event-patterns-table-body"></tbody>
										    </table>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			   	<?php include 'includes/footer.php'; ?>
				<!-- Scripts init -->
				<script type="text/javascript">
				    $(document).ready(function(){
				    	$("#epl-event-patterns-li").addClass("active");
				        loadEplEventPatternsData();
				        setInterval(loadEplEventPatternsData, 2000);
				    });
				</script>
				<!-- Paper Dashboard Core javascript -->
				<script src="assets/js/paper-dashboard.js" type="text/javascript"></script>
            </div>
        </div>
    </body>
</html>