<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta tags -->
    	<meta charset="utf-8"/>
    	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png"/>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="description" content="CEP Oficial Stats Site - By Ángel Gamaza"/>
        <meta name="author" content="Ángel Gamaza" />
        <meta name="copyright" content="Ángel Gamaza 2018" />
        <meta name="robots" content="noindex,nofollow"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <meta name="viewport" content="width=device-width" />

        <!-- Title tag -->
    	<title>CEP Stats Site</title>

        <!-- Bootstrap stylesheet -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!-- Paper Dashboard stylesheet -->
        <link href="assets/css/paper-dashboard.css" rel="stylesheet" type="text/css"/>

        <!-- Custom styles -->
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>

        <!-- Fonts and icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar" data-background-color="white" data-active-color="danger">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="home" class="simple-text">
                        <img src="assets/img/logo.png" width="15%" />CEP Stats Site
                        </a>
                    </div>
                    <ul class="nav">
                        <li id="event-types-li">
                            <a href="home">
                                <i class="ti-bar-chart"></i>
                                <p>Event Types</p>
                            </a>
                        </li>
                        <li id="epl-event-patterns-li">
                            <a href="epl-event-patterns">
                                <i class="ti-stats-up"></i>
                                <p>EPL Event Patterns</p>
                            </a>
                        </li>
                        <li id="complex-events-detected-li">
                            <a href="complex-events-detected">
                                <i class="ti-pie-chart"></i>
                                <p>Complex Events</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar bar1"></span>
                            <span class="icon-bar bar2"></span>
                            <span class="icon-bar bar3"></span>
                            </button>
                            <a class="navbar-brand" >Stats Panel</a>
                        </div>
                        <div class="collapse navbar-collapse"></div>
                    </div>
                </nav>