<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left hidden-xs">
            <ul>
                <li>
                    <a href="https://spring.io">
                    Spring Framework
                    </a>
                </li>
                <li>
                    <a href="http://www.espertech.com/">
                    EsperTech Engine
                    </a>
                </li>
                <li>
                    <a href="https://www.mulesoft.com/resources/esb/what-mule-esb">
                    Mule ESB
                    </a>
                </li>
                <li class="hidden-md">
                    <a href="https://www.mysql.com/">
                    MySQL Database
                    </a>
                </li>
                <li class="hidden-md">
                    <a href="https://www.mongodb.com/">
                    MongoDB Database
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script><i class="fa fa-heart heart"></i> by <a href="https://www.angelgamaza.es">Ángel Gamaza</a>
        </div>
    </div>
</footer>

<!-- Core JS Files -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js" type="text/javascript"></script>

<!-- Charts Plugin -->
<script src="assets/js/chartist.min.js" type="text/javascript"></script>

<!-- Consts definition -->
<script src="assets/js/consts.js" type="text/javascript"></script>

<!-- AJAX Calls -->
<script src="assets/js/ajax-calls.js" type="text/javascript"></script>