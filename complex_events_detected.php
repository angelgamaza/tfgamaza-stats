<?php include 'includes/header.php'; ?>
			    <div class="content">
			        <div class="container-fluid">
			            <div class="row">
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-success text-center">
			                                        <i class="ti-check"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Junta Events</p>
			                                        <span id="junta-complex-events-detected"></span>
			                                        <img class="mongo-complex-events-detected-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-danger text-center">
			                                        <i class="fa fa-times"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>MQTT Events</p>
			                                        <span id="mqtt-complex-events-detected"></span>
			                                        <img class="mongo-complex-events-detected-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-info text-center">
			                                        <i class="ti-server"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Another Events</p>
			                                        <span id="another-complex-events-detected"></span>
			                                        <img class="mongo-complex-events-detected-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-success text-center">
			                                        <i class="fas fa-server"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Total Events</p>
			                                        <span id="total-complex-events-detected"></span>
			                                        <img class="mongo-complex-events-detected-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="row">
			                <div class="col-md-6">
			                    <div class="card">
			                        <div class="header">
			                            <h4 class="title">Complex Events Detected Chart</h4>
			                        </div>
			                        <div class="content">
			                            <div id="complex-events-detected-chart" class="ct-chart ct-perfect-fourth">
											<img class="mongo-complex-events-detected-loading img-center" src="assets/img/loader.gif" width="25%">
			                            </div>
			                            <div class="footer">
			                                <div class="chart-legend">
			                                    <i class="fa fa-circle text-info"></i> JuntaAndalucia
			                                    <i class="fa fa-circle text-danger"></i> MQTT
			                                    <i class="fa fa-circle text-warning"></i> Another
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-md-6">
			                    <div class="card ">
			                        <div class="header">
			                            <h4 class="title">Complex Events Detected Stats</h4>
			                        </div>
			                        <div class="content">
			                        	<img class="mongo-complex-events-detected-loading img-center" src="assets/img/loader.gif" width="25%">
			                            <div class="table-responsive">
										    <table class="table" id="complex-events-detected-table">
										        <thead>
										            <tr>
										                <th scope="col">Detected By</th>
										                <th scope="col">Complex Events</th>
										                <th scope="col">Percentage</th>
										            </tr>
										        </thead>
										        <tbody id="complex-events-detected-table-body"></tbody>
										    </table>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>			            
			            <div class="row">
			                <div class="col-md-12">
			                    <div class="card ">
			                        <div class="header">
			                            <h4 class="title text-center"><strong>Last 10 Complex Events Detected</strong> <a class="download-link" onclick="generateCSV(3);"  data-toggle="tooltip" data-placement="top" title="Export complete historic in CSV"><i class="fas fa-download"></i></a></h4>
			                        </div>
			                        <div class="content">
			                        	<img class="mongo-complex-events-detected-loading img-center" src="assets/img/loader.gif" width="25%">
			                            <div class="table-responsive" id="last-complex-events-detected-table">
										    <table class="table">
										        <thead>
										            <tr>
										                <th scope="col" width="25%">Detected By</th>
										                <th scope="col">Content</th>
										                <th scope="col" width="25%">Reception date</th>
										            </tr>
										        </thead>
										        <tbody id="last-complex-events-detected-table-body"></tbody>
										    </table>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			   	<?php include 'includes/footer.php'; ?>
				<!-- Scripts init -->
				<script type="text/javascript">
				    $(document).ready(function(){
				    	$("#complex-events-detected-li").addClass("active");
				        loadComplexEventsDetectedData();
				        setInterval(loadComplexEventsDetectedData, 2000);
				    });
				</script>
				<!-- Paper Dashboard Core javascript -->
				<script src="assets/js/paper-dashboard.js" type="text/javascript"></script>
            </div>
        </div>
    </body>
</html>