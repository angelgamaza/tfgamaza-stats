/** Load Event Types content **/
function loadEventTypesData(){
	// Load MySQL Event Types
    $.ajax({
        url: base_url + "/esper/event_types/",
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader('Authorization', 'Basic ZXNwZXI6VGZnYW1hemEuMjAxOCQ=');
        },
        complete: function(){
            $('.esper-event-types-loading:visible').hide();
        },
        error: function(){
            // Put zero values if API not connected
            $("#total-event-types").html(0);
            $("#enabled-event-types").html(0);
            $("#disabled-event-types").html(0);
            // Hide Charts
            $('#event-types-deployment-chart').removeClass("ct-chart");
            // Hide Event Types Table
            $('#esper-event-types-table:visible').hide();
        },
        success: function(data){
        	// Event Types counters
        	var totalEventTypes = data.length;
            var enabledEventTypes = 0, disabledEventTypes = 0;

            // Count enabled/disabled Event Types
            $.each(data, function(key, value) {
                if(value.enabled)
                    ++enabledEventTypes;
                else
                	++disabledEventTypes;
            });

            // Get pie chart percentages
            var enabledEventTypesPercentage = (data.length === 0) ? 0 : Math.round((enabledEventTypes/totalEventTypes) * 100);
            var disabledEventTypesPercentage = (data.length === 0) ? 0 : 100 - enabledEventTypesPercentage;

            // Update pie chart
            Chartist.Pie('#event-types-deployment-chart', {
                labels: [enabledEventTypesPercentage + '%', disabledEventTypesPercentage + '%'],
                series: [enabledEventTypesPercentage, disabledEventTypesPercentage]
            });

            // Add class to chart to show it
            if(!$("#event-types-deployment-chart").hasClass("ct-chart"))
                $('#event-types-deployment-chart').addClass("ct-chart");

            // Update Event Types Table
            var enabledEventTypesTable = '<tr><th scope="row">Enabled <i class="fa fa-circle text-info"></i></th><td>' + enabledEventTypes + '</td><td>' + enabledEventTypesPercentage + '%</td></tr>\
                <tr><th scope="row">Disabled <i class="fa fa-circle text-danger"></i></th><td>' + disabledEventTypes + '</td><td>' + disabledEventTypesPercentage + '%</td></tr>\
                <tr><th scope="row"></th><td>' + totalEventTypes + '</td><td>100%</td></tr>';

            // Update Event Types enabled/disabled values
            $("#total-event-types").html(totalEventTypes);
            $("#enabled-event-types").html(enabledEventTypes);
            $("#disabled-event-types").html(disabledEventTypes);
            $("#esper-event-types-table-body").html(enabledEventTypesTable);

            // Show Event Types Table
            $('#esper-event-types-table:hidden').show();
        }
    });

	// Load Mongo Event Types
    $.ajax({
        url: base_url + "/mongo/event_types/",
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader('Authorization', 'Basic bW9uZ286VGZnYW1hemEuMjAxOCQ=');
        },
        complete: function(){
            $('.mongo-event-types-loading:visible').hide();
        },
        error: function(){
            // Put zero values if API not connected
            $("#received-event-types").html(0);
            // Hide Charts
            $('#received-events-chart').removeClass("ct-chart");
            // Hide Received Events Table
            $('#mongo-received-events-table:visible').hide();
            // Hide Received Events Table
            $('#mongo-last-received-events-table:visible').hide();
        },
        success: function(data){
        	// Received Events counters
        	var totalEvents = data.length;
            var juntaEvents = 0, mqttEvents = 0, anotherEvents = 0;

            // Create Last Received Events Table
            var lastReceivedEventsTable = '';

            // Get Received Events counts
            $.each(data, function(key, value) {
                // Fill table
                if(key < 10)
                    lastReceivedEventsTable += '<tr><th scope="row"> <i class="fa fa-check text-success"></i>&nbsp; ' + value.name + '</th><td class="hidden-xs">' + value.content.substring(0, 100) + ' ...</td><td class="visible-xs">' + value.content.substring(0, 30) + ' ...</td><td>' + value.insertionDate + '</td></tr>';
                // Set countes
                if(value.name === "JuntaAndalucia")
                    ++juntaEvents;
                else if(value.name === "MQTT")
                    ++mqttEvents;
                else
                    ++anotherEvents;
            });

            var juntaEventsPercentage = (data.length === 0) ? 0 : Math.round((juntaEvents/totalEvents) * 100);
            var mqttEventsPercentage = (data.length === 0) ? 0 : Math.round((mqttEvents/totalEvents) * 100);
            var anotherEventsPercentage = (data.length === 0) ? 0 : Math.round((anotherEvents/totalEvents) * 100);

            // Update Received Events Table
            var receivedEventsTable = '<tr><th scope="row">JuntaAndalucia <i class="fa fa-circle text-info"></i></th><td>' + juntaEvents + '</td><td>' + juntaEventsPercentage + '%</td></tr>\
                <tr><th scope="row">MQTT<i class="fa fa-circle text-danger"></i></th><td>' + mqttEvents + '</td><td>' + mqttEventsPercentage + '%</td></tr>\
                <tr><th scope="row">Another <i class="fa fa-circle text-warning"></i></th><td>' + anotherEvents + '</td><td>' + anotherEventsPercentage + '%</td></tr>';

            // Update Received Events values
            $("#received-event-types").html(totalEvents);
            $("#mongo-received-events-table-body").html(receivedEventsTable);
            
            // If no elements, show nothing
            if(data.length !== 0){
                $("#mongo-last-received-events-table-body").html(lastReceivedEventsTable);
                $('#mongo-last-received-events-table:hidden').show();
                $("#received-events-chart").addClass("ct-chart");

                // Update pie chart
                Chartist.Pie('#received-events-chart', {
                    labels: [juntaEventsPercentage + '%', mqttEventsPercentage + '%', anotherEventsPercentage + '%'],
                    series: [juntaEventsPercentage, mqttEventsPercentage, anotherEventsPercentage]
                });

                // Add class to chart to show it
                if(!$("#received-events-chart").hasClass("ct-chart"))
                    $('#received-events-chart').addClass("ct-chart");
            }
            else 
                $("#received-events-chart").removeClass("ct-chart");

            // Show Received Events Table
            $('#mongo-received-events-table:hidden').show();
        }
    });

}

/** Load EPL Event Patterns content **/
function loadEplEventPatternsData(){
    // Load Esper EPL Event Patterns
    $.ajax({
        url: base_url + "/esper/epl_event_patterns/",
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader('Authorization', 'Basic ZXNwZXI6VGZnYW1hemEuMjAxOCQ=');
        },
        complete: function(){
            $('.esper-epl-event-patterns-loading:visible').hide();
        },
        error: function(){
            // Put zero values if API not connected
            $("#total-epl-event-patterns").html(0);
            $("#deployment-epl-event-patterns").html(0);
            $("#inesper-epl-event-patterns").html(0);
            // Hide Charts
            $('#epl-event-patterns-deployment-chart').removeClass("ct-chart");
            $('#epl-event-patterns-inesper-chart').removeClass("ct-chart");
            // Hide Event Types Table
            $('#esper-deployment-epl-event-patterns-table:visible').hide();
            $('#esper-engine-epl-event-patterns-table:visible').hide();
        },
        success: function(data){
            // EPL Event Patterns counters
            var totalEplEventPatterns = data.length;
            var deployedEplEventPatterns = 0, undeployedEplEventPatterns = 0;
            var inEsperEplEventPatterns = 0, notInEsperEplEventPatterns = 0;

            // Count deployed/undeployed EPL Event Patterns
            $.each(data, function(key, value) {
                if(value.deployed)
                    ++deployedEplEventPatterns;
                else
                    ++undeployedEplEventPatterns;
                if(value.inEsper)
                    ++inEsperEplEventPatterns;
                else
                    ++notInEsperEplEventPatterns;
            });

            // Get pie charts percentages
            var deployedEplEventPatternsPercentage = (data.length === 0) ? 0 : Math.round((deployedEplEventPatterns/totalEplEventPatterns) * 100);
            var undeployedEplEventPatternsPercentage = (data.length === 0) ? 0 : 100 - deployedEplEventPatternsPercentage;
            var inEsperEplEventPatternsPercentage = (data.length === 0) ? 0 : Math.round((inEsperEplEventPatterns/totalEplEventPatterns) * 100);
            var notInEsperEplEventPatternsPercentage = (data.length === 0) ? 0 : 100 - inEsperEplEventPatternsPercentage;

            // Update EPL Event Patterns Tables
            var deploymentEplEventPatternsTable = '<tr><th scope="row">Deployed <i class="fa fa-circle text-info"></i></th><td>' + deployedEplEventPatterns + '</td><td>' + deployedEplEventPatternsPercentage + '%</td></tr>\
                <tr><th scope="row">Undeployed <i class="fa fa-circle text-danger"></i></th><td>' + undeployedEplEventPatterns + '</td><td>' + undeployedEplEventPatternsPercentage + '%</td></tr>\
                <tr><th scope="row"></th><td>' + totalEplEventPatterns + '</td><td>100%</td></tr>';

            var inEsperEplEventPatternsTable = '<tr><th scope="row">In Esper <i class="fa fa-circle text-info"></i></th><td>' + inEsperEplEventPatterns + '</td><td>' + inEsperEplEventPatternsPercentage + '%</td></tr>\
                <tr><th scope="row">Undeployed <i class="fa fa-circle text-danger"></i></th><td>' + notInEsperEplEventPatterns + '</td><td>' + notInEsperEplEventPatternsPercentage + '%</td></tr>\
                <tr><th scope="row"></th><td>' + totalEplEventPatterns + '</td><td>100%</td></tr>';

            // Update Event Types deployed/undeployed values
            $("#total-epl-event-patterns").html(totalEplEventPatterns);
            $("#deployment-epl-event-patterns").html(deployedEplEventPatterns + " / " + undeployedEplEventPatterns);
            $("#inesper-epl-event-patterns").html(inEsperEplEventPatterns + " / " + notInEsperEplEventPatterns);
            $("#esper-deployment-epl-event-patterns-table-body").html(deploymentEplEventPatternsTable);
            $("#esper-engine-epl-event-patterns-table-body").html(inEsperEplEventPatternsTable);

            // Update pie charts
            Chartist.Pie('#epl-event-patterns-deployment-chart', {
                labels: [deployedEplEventPatternsPercentage + '%', undeployedEplEventPatternsPercentage + '%'],
                series: [deployedEplEventPatternsPercentage, undeployedEplEventPatternsPercentage]
            });

            // Add class to chart to show it
            if(!$("#epl-event-patterns-deployment-chart").hasClass("ct-chart"))
                $('#epl-event-patterns-deployment-chart').addClass("ct-chart");

            Chartist.Pie('#epl-event-patterns-inesper-chart', {
                labels: [inEsperEplEventPatternsPercentage + '%', notInEsperEplEventPatternsPercentage + '%'],
                series: [inEsperEplEventPatternsPercentage, notInEsperEplEventPatternsPercentage]
            });

            // Add class to chart to show it
            if(!$("#epl-event-patterns-inesper-chart").hasClass("ct-chart"))
                $('#epl-event-patterns-inesper-chart').addClass("ct-chart");

            // Show Event Types Table
            $('#esper-deployment-epl-event-patterns-table:hidden').show();
            $('#esper-engine-epl-event-patterns-table:hidden').show();
        }
    });

    // Load Mongo EPL Event Patterns
    $.ajax({
        url: base_url + "/mongo/epl_event_patterns/",
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader('Authorization', 'Basic bW9uZ286VGZnYW1hemEuMjAxOCQ=');
        },
        complete: function(){
            $('.mongo-epl-event-patterns-loading:visible').hide();
        },
        error: function(){
            // Put zero values if API not connected
            $("#incorrect-epl-event-patterns").html(0);
        },
        success: function(data){
            // Incorrect EPL Event Patterns counters
            var totalIncorrectEplEventPatterns = data.length;

            // Create Incorrect Event Patterns Table
            if(data.length !== 0){
                var incorrectEplEventPatternsTable = '';
                $.each(data, function(key, value) {
                   incorrectEplEventPatternsTable += '<tr><th scope="row"> <i class="fa fa-times text-danger"></i>&nbsp; ' + value.name + '</th><td class="hidden-xs">' + value.content.substring(0, 180) + ' ...</td><td class="visible-xs">' + value.content.substring(0, 180) + ' ...</td><td>' + value.insertionDate + '</td></tr>';
                   return key < 10;
                });

                // Update Incorrect Event Patterns values
                $("#mongo-incorrect-epl-event-patterns-table-body").html(incorrectEplEventPatternsTable);

                // Show Incorrect EPL Event Patterns Table
                $('#mongo-incorrect-epl-event-patterns-table:hidden').show();
            }
            // Update Incorrect Event Patterns values
            $("#incorrect-epl-event-patterns").html(totalIncorrectEplEventPatterns);
        }
    });

}

/** Load Complex Events Detected content **/
function loadComplexEventsDetectedData(){
    // Load Complex Events Detected
    $.ajax({
        url: base_url + "/mongo/complex_events_detected/",
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader('Authorization', 'Basic bW9uZ286VGZnYW1hemEuMjAxOCQ=');
        },
        complete: function(){
            $('.mongo-complex-events-detected-loading:visible').hide();
        },
        error: function(){
            // Put zero values if API not connected
            $("#junta-complex-events-detected").html(0);
            $("#mqtt-complex-events-detected").html(0);
            $("#another-complex-events-detected").html(0);
            $("#total-complex-events-detected").html(0);

            // Hide Charts
            $('#complex-events-detected-chart').removeClass("ct-chart")

            // Hide Complex Events Detected Tables
            $('#complex-events-detected-table:visible').hide();
            $('#last-complex-events-detected-table:visible').hide();

        },
        success: function(data){
            // Complex Events Detected counters
            var totalComplexEventDetected = data.length;
            var juntaEvents = 0, mqttEvents = 0, anotherEvents = 0;

            // Create Complex Events Detected Table
            var lastComplexEventsDetectedTable = '';
            $.each(data, function(key, value) {
                // Fill table
                if(key < 10)
                    lastComplexEventsDetectedTable += '<tr><th scope="row"> <i class="far fa-bell text-warning"></i>&nbsp; ' + value.detectedBy + '</th><td class="hidden-xs">' + value.detectedEvent.substring(0, 100) + ' ...</td><td class="visible-xs">' + value.detectedEvent.substring(0, 30) + ' ...</td><td>' + value.insertionDate + '</td></tr>';
                // Get Complex Events Detected by Event Type
                if(value.detectedBy === "JuntaAndalucia")
                    ++juntaEvents;
                else if(value.detectedBy === "MQTT")
                    ++mqttEvents;
                else
                    ++anotherEvents;
            });

            // Get pie charts percentages
            var juntaComplexEventsDetectedPercentage = (data.length === 0) ? 0 : Math.round((juntaEvents/totalComplexEventDetected) * 100);
            var mqttComplexEventsDetectedPercentage = (data.length === 0) ? 0 : Math.round((mqttEvents/totalComplexEventDetected) * 100);
            var anotherComplexEventsDetectedPercentage = (data.length === 0) ? 0 : Math.round((anotherEvents/totalComplexEventDetected) * 100);

            // If no elements, show nothing
            if(data.length !== 0){

                // Add class to chart to show it
                if(!$('#complex-events-detected-chart').addClass("ct-chart"))
                    $('#complex-events-detected-chart').addClass("ct-chart")

                // Update Pie Chart
                Chartist.Pie('#complex-events-detected-chart', {
                    labels: [juntaComplexEventsDetectedPercentage + '%', mqttComplexEventsDetectedPercentage + '%', anotherComplexEventsDetectedPercentage + '%'],
                    series: [juntaComplexEventsDetectedPercentage, mqttComplexEventsDetectedPercentage, anotherComplexEventsDetectedPercentage]
                });
            }
            else
                $('#complex-events-detected-chart').removeClass("ct-chart")

            // Update Complex Events Detected Table
            var complexEventsDetectedTable = '<tr><th scope="row">JuntaAndalucia <i class="fa fa-circle text-info"></i></th><td>' + juntaEvents + '</td><td>' + juntaComplexEventsDetectedPercentage + '%</td></tr>\
                <tr><th scope="row">MQTT<i class="fa fa-circle text-danger"></i></th><td>' + mqttEvents + '</td><td>' + mqttComplexEventsDetectedPercentage + '%</td></tr>\
                <tr><th scope="row">Another <i class="fa fa-circle text-warning"></i></th><td>' + anotherEvents + '</td><td>' + anotherComplexEventsDetectedPercentage + '%</td></tr>\
                <tr><th scope="row"></th><td>' + totalComplexEventDetected + '</td><td>100%</td></tr>';

            // Update Complex Events Detected values
            $("#junta-complex-events-detected").html(juntaEvents);
            $("#mqtt-complex-events-detected").html(mqttEvents);
            $("#another-complex-events-detected").html(anotherEvents);
            $("#total-complex-events-detected").html(totalComplexEventDetected);
            $("#complex-events-detected-table-body").html(complexEventsDetectedTable);

            // If no elements, show nothing
            if(data.length !== 0){
                $("#last-complex-events-detected-table-body").html(lastComplexEventsDetectedTable);
                $('#last-complex-events-detected-table:hidden').show();
            }

            // Show Complex Events Detected Tables
            $('#complex-events-detected-table:hidden').show();
        }
    });

}

/** New functions **/

function generateCSV(source){
    // Global variables
    var url = "";
    var filename = "";
    var schema = "";
    var fields = [];
    // Switch case
    switch(source) {
        // Event Types
        case 1:
            url = base_url + "/mongo/event_types/";
            filename = "event_types.csv";
            schema = "id; name; content; reception date \n";
            fields = ["id", "name", "content", "insertionDate"];
            break;
        // EPL Event Patterns
        case 2:
            url = base_url + "/mongo/epl_event_patterns/";
            filename = "epl_event_patterns.csv";
            schema = "id; name; content; exception date \n";
            fields = ["id", "name", "content", "insertionDate"];
            break;
        // Complex Events Detected
        case 3:
            url = base_url + "/mongo/complex_events_detected/";
            filename = "complex_events_detected.csv";
            schema = "id; detected by; detected event; detection date \n";
            fields = ["id", "detectedBy", "detectedEvent", "insertionDate"];
            break;
        default:
            break;
    }
    // Get CSV data from server
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function(req) {
            req.setRequestHeader('Authorization', 'Basic bW9uZ286VGZnYW1hemEuMjAxOCQ=');
        },
        success: function(data){
            //Generate CSV content
            var csvData = schema;
            $.each(data, function(key, value) {
                csvData += (value[fields[0]] + "; " + value[fields[1]] + "; " + value[fields[2]] + "; " + value[fields[3]] + " \n");
            });

            // Create hidden element for CSV download
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvData);
            hiddenElement.target = '_blank';
            hiddenElement.download = filename;
            hiddenElement.click();
        },
        error: function(){
            console.log("An error has occurred");
        }
    }); 
}
