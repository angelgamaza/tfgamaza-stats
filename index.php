<?php include 'includes/header.php'; ?>
			    <div class="content">
			        <div class="container-fluid">
			            <div class="row">
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-success text-center">
			                                        <i class="ti-check"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Enabled E. Types</p>
			                                        <span id="enabled-event-types"></span>
			                                        <img class="esper-event-types-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-danger text-center">
			                                        <i class="fa fa-times"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Disabled E. Types</p>
			                                        <span id="disabled-event-types"></span>
			                                        <img class="esper-event-types-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-info text-center">
			                                        <i class="ti-server"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Event Types Total</p>
			                                        <span id="total-event-types"></span>
			                                        <img class="esper-event-types-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-lg-3 col-sm-6">
			                    <div class="card">
			                        <div class="content">
			                            <div class="row">
			                                <div class="col-xs-3">
			                                    <div class="icon-big icon-success text-center">
			                                        <i class="fas fa-server"></i>
			                                    </div>
			                                </div>
			                                <div class="col-xs-9">
			                                    <div class="numbers">
			                                        <p>Received Events</p>
			                                        <span id="received-event-types"></span>
			                                        <img class="mongo-event-types-loading" src="assets/img/loader.gif" width="20%">
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="row">
			                <div class="col-md-6">
			                	<div class="row">
			                		<div class="col-md-12">
					                    <div class="card">
					                        <div class="header">
					                            <h4 class="title">Activation Stats Chart</h4>
					                        </div>
					                        <div class="content">
					                            <div id="event-types-deployment-chart" class="ct-chart ct-perfect-fourth">
													<img class="esper-event-types-loading img-center" src="assets/img/loader.gif" width="25%">
					                            </div>
					                            <div class="footer">
					                                <div class="chart-legend">
					                                    <i class="fa fa-circle text-info"></i> Enabled
					                                    <i class="fa fa-circle text-danger"></i> Disabled
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					            <div class="row">
			                		<div class="col-md-12">
					                    <div class="card ">
					                        <div class="header">
					                            <h4 class="title">Received Events Stats</h4>
					                        </div>
					                        <div class="content">
					                        	<img class="mongo-event-types-loading img-center" src="assets/img/loader.gif" width="25%">
					                            <div class="table-responsive" id="mongo-received-events-table">
												    <table class="table">
												        <thead>
												            <tr>
												                <th scope="col">Event Type</th>
												                <th scope="col">Events</th>
												                <th scope="col">Percentage</th>
												            </tr>
												        </thead>
												        <tbody id="mongo-received-events-table-body"></tbody>
												    </table>
												</div>
					                        </div>
					                    </div>
					                </div>
					            </div>
			                </div>
			                <div class="col-md-6">
			                	<div class="row">
					            	<div class="col-md-12">
					                    <div class="card ">
					                        <div class="header">
					                            <h4 class="title">Activation Stats</h4>
					                        </div>
					                        <div class="content">
					                        	<img class="esper-event-types-loading img-center" src="assets/img/loader.gif" width="25%">
					                            <div class="table-responsive">
												    <table class="table" id="esper-event-types-table">
												        <thead>
												            <tr>
												                <th scope="col">Status</th>
												                <th scope="col">Event Types</th>
												                <th scope="col">Percentage</th>
												            </tr>
												        </thead>
												        <tbody id="esper-event-types-table-body"></tbody>
												    </table>
												</div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					            <div class="row">
					            	<div class="col-md-12">
					                    <div class="card">
					                        <div class="header">
					                            <h4 class="title">Received Events Chart</h4>
					                        </div>
					                        <div class="content">
					                            <div id="received-events-chart" class="ct-chart ct-perfect-fourth">
													<img class="mongo-event-types-loading img-center" src="assets/img/loader.gif" width="25%">
					                            </div>
					                            <div class="footer">
					                                <div class="chart-legend">
					                                    <i class="fa fa-circle text-info"></i> JuntaAndalucia
					                                    <i class="fa fa-circle text-danger"></i> MQTT
					                                    <i class="fa fa-circle text-warning"></i> Another
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					            	</div>
					            </div>
			                </div>
			            </div>			            
			            <div class="row">
			                <div class="col-md-12">
			                    <div class="card ">
			                        <div class="header">
			                            <h4 class="title text-center"><strong>Last 10 Events Received</strong> <a class="download-link" onclick="generateCSV(1);"  data-toggle="tooltip" data-placement="top" title="Export complete historic in CSV"><i class="fas fa-download"></i></a></h4>
			                        </div>
			                        <div class="content">
			                        	<img class="mongo-event-types-loading img-center" src="assets/img/loader.gif" width="25%">
			                            <div class="table-responsive" id="mongo-last-received-events-table">
										    <table class="table">
										        <thead>
										            <tr>
										                <th scope="col" width="25%">Name</th>
										                <th scope="col">Content</th>
										                <th scope="col" width="25%">Reception date</th>
										            </tr>
										        </thead>
										        <tbody id="mongo-last-received-events-table-body"></tbody>
										    </table>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			   	<?php include 'includes/footer.php'; ?>
				<!-- Scripts init -->
				<script type="text/javascript">
				    $(document).ready(function(){
				    	$("#event-types-li").addClass("active");
				        loadEventTypesData();
				        setInterval(loadEventTypesData, 2000);
				    });
				</script>
				<!-- Paper Dashboard Core javascript -->
				<script src="assets/js/paper-dashboard.js" type="text/javascript"></script>
            </div>
        </div>
    </body>
</html>